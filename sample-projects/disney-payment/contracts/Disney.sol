// SPDX-License-Identifier: MIT

pragma solidity >0.5.0 <0.8.0;
pragma experimental ABIEncoderV2;
import "./library/ERC20Basic/ERC20Basic.sol";

contract Disney {

    //--------------------- DECLARACIONES INICIALES ------------------------------------------

    // instancia del token
    ERC20Basic private token;
    
    // Direccion del ownwer (Disney)
    address payable public owner;

    // estructura de datos para almacenar los clientes
    struct cliente {
        uint tokensComprados;
        string[] atraccionesDisfrutadas;
    }

    // asocia un address con un cliente (estructura)
    mapping (address => cliente) public Clientes;

    constructor () {
        token = new ERC20Basic(10000);
        owner = msg.sender;
    }

    //--------------------- GESTION DE TOKENS ------------------------------------------
    
    // establece el precio del token en base a ether
    function precioToken(uint cantidadTokens) internal pure returns(uint) {
        return cantidadTokens*(1 ether);
    }

    // obtiene el numero de tokens disponibles en el contrato para ser utilizados
    function balanceOf() public view returns(uint) {
        // verifica el balance del contrato
        // address(this) nos devuelve la direccion del contrato
        return token.balanceOf(address(this));
    }

    // permite comprar tokens
    function comprarTokens(uint _cantidadTokens) public payable {
        // obtengo el precio del token
        uint precio = precioToken(_cantidadTokens);

        // se evalua el dinero que el cliente paga por el token
        require(msg.value >= precio, "compre menos tokens o cargue mas ether");
        
        // si el valor que paga el cliente es mayor al precio
        // se devuelve esa diferencia
        uint returnValue = msg.value - precio;

        // devuelve al sender el "vuelto"
        msg.sender.transfer(returnValue);

        // obtento el numero de tokens disponibles
        uint balance = balanceOf();
        require(_cantidadTokens <= balance, "Compra un numero menor de tokens");

        // se transfiere el numero de tokens al cliente
        token.transfer(msg.sender, _cantidadTokens);

        //registro de tokens comprados
        Clientes[msg.sender].tokensComprados = _cantidadTokens;
    }
}