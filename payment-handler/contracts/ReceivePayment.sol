// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

import "./access/Ownable.sol";

contract ReceivePayment is Ownable {

  address payable depositsAccount; 
  event paymanet(address from, address to, uint amount, uint order);

  constructor() {
    depositsAccount = payable(owner());
  }

  function deposit() payable public {
    (bool success,) = depositsAccount.call{value: _msgValue()}("");
    require(success, "failed to send money");
  }

  function payOrder(uint256 _amount, uint64 _orderNumber) payable public {
    require(_msgValue() == _amount, "Amount should be equals to value");
    (bool success,) = depositsAccount.call{value: _msgValue()}("");
    require(success, "failed to send money");
    emit paymanet(_msgSender(), address(this), _amount, _orderNumber);
  }

  function getBalance() public view returns (uint256) {
    return address(depositsAccount).balance;
  }

  function setBalanceAccount(address payable _account) public onlyOwner {
    depositsAccount = _account;
  }

  function getBalanceAccount() public view returns (address) {
    return depositsAccount;
  }

}