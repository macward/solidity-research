// SPDX-License-Identifier: MIT
pragma solidity >=0.4.22 <0.9.0;

contract SimpleStorage {

  uint private data;
  event dataWrite(address _from, address _to, uint _value, uint _code);

  function updateData(uint _data) external {
    data = _data;
    emit dataWrite(msg.sender, address(this), _data, block.number);
  }

  function readData() external view returns(uint) {
    return data;
  }
  // Deployed at BSC Testnet at address:
  // 0xDD360F98Ff4efF2eFE2D8b9f38a29323Fb6101D2 (deprecated)
  // 0x42201a935E3df3Bfe8D79b4Af107F1DCA33e9D3a (new with events)
}