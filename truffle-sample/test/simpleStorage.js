const { assert } = require("console");

const SimpleStorage = artifacts.require('SimpleStorage');

contract('Storage', async () => {
  const storage = await SimpleStorage.new();
  storage.updateData(10);
  const data = storage.readData();
  assert(data.toString() === '10');
  assert(data.toNumber() === 2);
});