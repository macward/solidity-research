const Web3API = require('web3');
const { readFileSync } = require('fs');

var web3 = new Web3API(new Web3API.providers.HttpProvider('http://localhost:7545'));
const contractJson = readFileSync('../build/contracts/SimpleStorage.json');
const artifact = JSON.parse(contractJson);
// const accounts = web3.eth.getAccounts().then(console.log);
let contract = new web3.eth.Contract(artifact['abi'], '0xD46Ee239142F550F09eEc2a606e736a67a96D40a');

// contract.methods.updateData(11).send({ from: '0x1A77eF632e75c25c3c54BE28fceF6309Aaf49A94' }).then(console.log);
const contractData = await getData(contract);
console.log(contractData);
// 0xD46Ee239142F550F09eEc2a606e736a67a96D40a

async function getData(contract) {
  const value = await contract.methods.readData().call();
  return value
}